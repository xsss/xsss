-----------------------------
xsss - Suppress X screensaver
-----------------------------

Simple utility that disables the X11 screensaver and 
DPMS power saving while running.

SYNOPSIS
       xsss [-i interval] [-t times] [-w command]

OPTIONS
       -i|--interval <seconds> - Interval at which to supress the screensaver. The default is 120s.

       -t|--times <integer> - Number of intervals to run. The default is to run until the program is killed. Cannot be combined with the -w option.

       -w|--wrap <command...> - "Executes" the rest of the command line. Terminates when the spawned program is done.

Copyright (c) 2011 Christian Henz <chrhenz@gmx.de>
Released unter the GNU General Public License V3 or later (see LICENSE.txt)
