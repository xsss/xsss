SOURCES = xsss.c
OBJECTS = $(patsubst %.c, %.o, $(SOURCES))

CFLAGS = -Wall -O0 -g -std=gnu99
LDFLAGS = $(shell pkg-config x11 xext xscrnsaver --libs)

depend-all:
	$(MAKE) depend
	$(MAKE) all

depend:
	$(CC) $(CFLAGS) -M $(SOURCES) > .depend

all: $(OBJECTS)
	$(CC) -o xsss $(OBJECTS) $(LDFLAGS)

clean:
	$(RM) $(OBJECTS) xsss .depend

$(OBJECTS): Makefile

.PHONY: depend-all depend all clean

-include .depend
