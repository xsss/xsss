/*
  xsss - suppress X screensaver

  Copyright (C) 2011 Christian Henz

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/dpms.h>
#include <X11/extensions/scrnsaver.h>

#define PRINT_INFO 1

void queryDPMS( Display* dpy, CARD16* powerLevel, BOOL* state )
{
  DPMSInfo( dpy, powerLevel, state );
#if PRINT_INFO
  fprintf( stderr, 
	   "DPMS state:\n"
	   "  power level = %d\n"
	   "  state = %s\n", *powerLevel, *state ? "true" : "false" );
#endif
}

void disableDPMS( Display* dpy )
{
  CARD16 powerLevel;
  BOOL state;

  queryDPMS( dpy, &powerLevel, &state );
  if( state ) {
    
    DPMSDisable( dpy );
  }
}

static int g_quit = 0;
void sigHandler( int sig )
{
  fprintf( stderr, "Signal received: %d\n", sig );
  g_quit = 1;
}

int main( int argc, char** argv )
{
  // Install signal handler to allow graceful termination...
  struct sigaction ssa;
  ssa.sa_handler = sigHandler;
  sigemptyset( &ssa.sa_mask );
  ssa.sa_flags = 0;
  sigaction( SIGINT, &ssa, 0 );
  sigaction( SIGTERM, &ssa, 0 );
  sigaction( SIGCHLD, &ssa, 0 );

  // Parse command line arguments...

  int interval = 120; // Default interval
  char* wrapCmd = NULL;
  char** wrapArgv = NULL;
  int times = -1;

  for( int i = 1; i < argc; ++i ) {

    char* nextArg = ( i + 1 < argc ) ? argv[ i + 1 ] : 0;

    if( strcmp( "-i", argv[ i ] ) == 0 ||
	strcmp( "--interval", argv[ i ] ) == 0 ) {

      if( !nextArg ) {

	fprintf( stderr, "ERROR: Option '%s' needs argument!\n", argv[ i ] );
	exit( 1 );
      }

      int tmp = atoi( nextArg );
      if( tmp <= 0 ) {

	fprintf( stderr, "ERROR: Invalid argument for option '%s'!\n", argv[ i ] );
	exit( 1 );
      }
      interval = tmp;

      ++i;
      continue;
    }
    if( strcmp( "-w", argv[ i ] ) == 0 ||
	strcmp( "--wrap", argv[ i ] ) == 0 ) {

      if( !nextArg ) {
	
	fprintf( stderr, "ERROR: Option '%s' needs a command to run!\n", argv[ i ] );
	exit( 1 );
      }

      wrapCmd = nextArg;
      wrapArgv = &argv[ i + 1 ];
      break;
    }
    if( strcmp( "-t", argv[ i ] ) == 0 ||
	strcmp( "--times", argv[ i ] ) == 0 ) {

      if( !nextArg ) {
	
	fprintf( stderr, "ERROR: Option '%s' needs a command to run!\n", argv[ i ] );
	exit( 1 );
      }

      times = atoi( nextArg );

      ++i;
    }
    else {

      fprintf( stderr, "ERROR: Unknown option '%s'!\n", argv[ i ] );
      exit( 1 );
    }
  }

  // Run in wrap mode?
  if( wrapCmd ) {

    pid_t child = fork();
    if( !child ) {
      
      if( execv( wrapCmd, wrapArgv ) == - 1 ) {

	perror( "ERROR: execv:" );
	exit( 1 );
      }
    }
    else if( child == -1 ) {

      perror( "ERROR: fork:" );
      exit( 1 );
    }
    else {

      fprintf( stderr, "Child spawned: %d\n", child );
    }
  }
  
  // Connect to X server...
  Display* dpy = XOpenDisplay( 0 );
  if( !dpy ) {

    fprintf( stderr, "ERROR: XOpenDisplay\n" );
    exit( 1 );
  }

#if PRINT_INFO
  int xssTimeout;
  int xssInterval;
  int xssPreferBlanking;
  int xssAllowExposures;
  XGetScreenSaver( dpy, &xssTimeout, &xssInterval, &xssPreferBlanking, &xssAllowExposures );
  fprintf( stderr, 
	   "Screensaver state:\n"
	   "  timeout = %d\n"
	   "  interval = %d\n"
	   "  prefer blanking = %d\n"
	   "  allow exposures = %d\n", xssTimeout, xssInterval, xssPreferBlanking, xssAllowExposures );
#endif

  // Store original state of DPMS...
  CARD16 dpmsPowerLevel;
  BOOL dpmsState;
  queryDPMS( dpy, &dpmsPowerLevel, &dpmsState );

  // Main loop...
  for( ; g_quit == 0; ) {

    disableDPMS( dpy );
    XResetScreenSaver( dpy );

    if( --times == 0 )
      break;
    
    if( sleep( interval ) )
      break;
  }

#if PRINT_INFO
  fprintf( stderr, "Quitting...\n" );
#endif

  // Clean up...

  // Restore original state of DPMS...
  if( dpmsState ) {

    DPMSEnable( dpy );
  }
  
  XCloseDisplay( dpy );

  return 0;
}
